defmodule Weather.Mixfile do
  use Mix.Project

  def project do
    [
      app: :weather,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [
        :httpoison,
        :logger,
        :meeseeks,
        :poison
      ]
    ]
  end

  defp deps do
    [
      {:httpoison, "~> 1.0.0"},
      {:meeseeks, "~> 0.7.6"},
      {:poison, "~> 3.1.0"}
    ]
  end
end
