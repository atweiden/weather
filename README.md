Weather
=======

Exercise from [Programming Elixir][pragprog] by Dave Thomas.


Description
-----------

> In the United States, the National Oceanic and Atmospheric
> Administration (NOAA) provides hourly XML feeds of conditions at 1,800
> [locations][obs].
>
>
> For example, the feed for a small airport close to where I’m writing
> this is at http://w1.weather.gov/xml/current_obs/KDTO.xml.
>
>
> Write an application that fetches this data, parses it, and displays
> it in a nice format.


Installation
------------

```sh
git clone https://gitlab.com/atweiden/weather && cd weather
mix deps.get
mix deps.compile
mix run -e 'Weather.CLI.run(["KDTO"])'
```


Licensing
---------

This is free and unencumbered public domain software. For more
information, see http://unlicense.org/ or the accompanying UNLICENSE file.


[pragprog]: https://pragprog.com/book/elixir/programming-elixir
[obs]: http://w1.weather.gov/xml/current_obs
