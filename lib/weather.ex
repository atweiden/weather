defmodule Weather do
  @moduledoc """
  # Weather

  Given an NOAA weather station ID, get its XML weather feed from
  weather.gov and pretty print it.
  """

  import Meeseeks.XPath

  @weather_url Application.get_env(:weather, :weather_url)

  # fetch {{{

  @doc """
  `Weather.fetch` takes an NOAA weather station ID and returns a formatted
  weather report from the station.
  """
  def fetch(station_id) when is_binary(station_id) do
    station_id
    |> url
    |> HTTPoison.get()
    |> format
  end

  # end fetch }}}
  # format {{{

  @doc """
  `Weather.format` takes an NOAA weather feed XML response from HTTPoison
  and parses it using `Meeseeks`
  """
  def format({:ok, %HTTPoison.Response{body: body, status_code: 200}}) do
    body
    |> Meeseeks.parse(:xml)
    |> Meeseeks.all(xpath("/*"))
    |> Enum.map(&Meeseeks.tree/1)
    |> _format
  end

  defp _format([{"current_observation", _version, current_observation}]) do
    current_observation
    |> Enum.map(&_format/1)
    |> Enum.filter(& &1)
    |> Enum.into(Map.new())
    |> Poison.encode!()
  end

  defp _format({"credit", _, [credit]}) do
    {:credit, credit}
  end

  defp _format({"credit_URL", _, [credit_url]}) do
    {:credit_url, credit_url}
  end

  defp _format({"location", _, [location]}) do
    {:location, location}
  end

  defp _format({"station_id", _, [station_id]}) do
    {:station_id, station_id}
  end

  defp _format({"latitude", _, [latitude]}) do
    {:latitude, String.to_float(latitude)}
  end

  defp _format({"longitude", _, [longitude]}) do
    {:longitude, String.to_float(longitude)}
  end

  defp _format({"observation_time", _, [observation_time]}) do
    {:observation_time, observation_time}
  end

  defp _format({"observation_time_rfc822", _, [observation_time_rfc822]}) do
    {:observation_time_rfc822, observation_time_rfc822}
  end

  defp _format({"weather", _, [weather]}) do
    {:weather, weather}
  end

  defp _format({"temperature_string", _, [temperature_string]}) do
    {:temperature_string, temperature_string}
  end

  defp _format({"temp_f", _, [temp_f]}) do
    {:temp_f, String.to_float(temp_f)}
  end

  defp _format({"temp_c", _, [temp_c]}) do
    {:temp_c, String.to_float(temp_c)}
  end

  defp _format({"relative_humidity", _, [relative_humidity]}) do
    {:relative_humidity, String.to_integer(relative_humidity)}
  end

  defp _format({"wind_string", _, [wind_string]}) do
    {:wind_string, wind_string}
  end

  defp _format({"wind_dir", _, [wind_dir]}) do
    {:wind_dir, wind_dir}
  end

  defp _format({"wind_degrees", _, [wind_degrees]}) do
    {:wind_degrees, String.to_integer(wind_degrees)}
  end

  defp _format({"wind_mph", _, [wind_mph]}) do
    {:wind_mph, String.to_float(wind_mph)}
  end

  defp _format({"wind_kt", _, [wind_kt]}) do
    {:wind_kt, String.to_integer(wind_kt)}
  end

  defp _format({"pressure_string", _, [pressure_string]}) do
    {:pressure_string, pressure_string}
  end

  defp _format({"pressure_mb", _, [pressure_mb]}) do
    {:pressure_mb, String.to_float(pressure_mb)}
  end

  defp _format({"pressure_in", _, [pressure_in]}) do
    {:pressure_in, String.to_float(pressure_in)}
  end

  defp _format({"dewpoint_string", _, [dewpoint_string]}) do
    {:dewpoint_string, dewpoint_string}
  end

  defp _format({"dewpoint_f", _, [dewpoint_f]}) do
    {:dewpoint_f, String.to_float(dewpoint_f)}
  end

  defp _format({"dewpoint_c", _, [dewpoint_c]}) do
    {:dewpoint_c, String.to_float(dewpoint_c)}
  end

  defp _format({"heat_index_string", _, [heat_index_string]}) do
    {:heat_index_string, heat_index_string}
  end

  defp _format({"heat_index_f", _, [heat_index_f]}) do
    {:heat_index_f, String.to_integer(heat_index_f)}
  end

  defp _format({"heat_index_c", _, [heat_index_c]}) do
    {:heat_index_c, String.to_integer(heat_index_c)}
  end

  defp _format({"visibility_mi", _, [visibility_mi]}) do
    {:visibility_mi, String.to_float(visibility_mi)}
  end

  defp _format({"ob_url", _, [ob_url]}) do
    {:ob_url, ob_url}
  end

  defp _format(_) do
    nil
  end

  # end format }}}
  # url {{{

  @doc """
  `Weather.url` accepts an NOAA weather station ID and returns the
  weather.gov URL for accessing its XML weather feed.

  ## Examples

      iex> Weather.url("KDTO")
      "http://w1.weather.gov/xml/current_obs/KDTO.xml"
  """
  def url(station_id) when is_binary(station_id) do
    "#{@weather_url}/#{station_id}.xml"
  end

  # end url }}}
end

# vim: set filetype=elixir foldmethod=marker foldlevel=0:
