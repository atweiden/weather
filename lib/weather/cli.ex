defmodule Weather.CLI do
  @moduledoc """
  # Weather.CLI

  The `Weather.CLI` module parses cmdline arguments, converts them to
  actions and returns the result on STDOUT.
  """

  # run {{{

  @doc """
  `Weather.CLI.run` accepts a list of arguments to process.

  ## Example

  iex> Weather.CLI.run(["KDTO"])
  :ok
  """
  def run(argv) when is_list(argv) do
    argv
    |> parse_args
    |> process
    |> String.trim()
    |> IO.puts()
  end

  # end run }}}
  # parse_args {{{

  @doc """
  `Weather.CLI.parse_args` parses cmdline args passed to the program.

  `argv` can be -h or --help, which returns `:help`.

  Otherwise it is an NOAA weather station ID.

  Returns a NOAA weather station ID, or `:help` if help was given.
  """
  def parse_args(argv) do
    argv
    |> OptionParser.parse(
      switches: [help: :boolean],
      aliases: [h: :help]
    )
    |> _parse_args
  end

  defp _parse_args({[help: true], _, _}) do
    :help
  end

  defp _parse_args({_, [station_id], _}) do
    station_id
  end

  defp _parse_args(_) do
    :help
  end

  # end parse_args }}}
  # process {{{

  defp process(:help) do
    """
    usage: weather <station_id>
    """
    |> String.trim()
    |> IO.puts()

    System.halt(0)
  end

  defp process(station_id) do
    Weather.fetch(station_id)
  end

  # end process }}}
end

# vim: set filetype=elixir foldmethod=marker foldlevel=0:
